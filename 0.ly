
\version "2.18.2"
% automatically converted by musicxml2ly from input_xml/0.xml

\header {
    }

\layout {
    \context { \Score
        autoBeaming = ##f
        }
    }
PartPOneVoiceNone =  \relative g {
    \clef "treble" \key c \major \time 1/128 g16*12 }


% The score definition
\score {
    <<
        \new Staff <<
            \set Staff.instrumentName = "Music"
            \context Staff << 
                \context Voice = "PartPOneVoiceNone" { \PartPOneVoiceNone }
                >>
            >>
        
        >>
    \layout {}
    % To create MIDI output, uncomment the following line:
    %  \midi {}
    }

