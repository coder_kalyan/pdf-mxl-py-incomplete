import xml
import subprocess
import os
from tqdm import tqdm

source_dir = "/input_xml"
destination_dir_b = "/input_png_b"
destination_dir_e = "/input_png_e"
destination_dir_g = "/input_png_g"

cwd = os.getcwd()

for file_name in tqdm(os.listdir(cwd + source_dir)):	
	file_source_path = cwd + source_dir + "/" + file_name
	file_save_path = cwd + destination_dir_g + "/" + file_name.split(".")[0] + ".png"
	file_converted_name_path = cwd + destination_dir_g + "/" + file_name.split(".")[0] + "-1.png"

	os.popen("mscore " + file_source_path + " -o " + file_save_path + " >/dev/null 2>&1")
	os.popen("convert -flatten " + file_converted_name_path + " " + file_converted_name_path)
	os.popen("convert -crop 150x600+300+250 " + file_converted_name_path + " " + file_save_path)
	

