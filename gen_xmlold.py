lbs = open("labels.txt", "w+")
names = ["A," "B", "C","D","E","F","G"]
octaves = [3, 4, 5, 6, 7]
durations = [1, 2, 3, 4, 5, 6, 7, 8]
types = ["128th", "64th", "32nd", "16th", "eighth", "quarter", "half", "whole"]
with open("template.xml", "r") as temp_f:
    template = temp_f.read()
    i = 0
    # print(template)
    for name in names:
        for octave in octaves:
            for duration, ty in zip(durations, types):
                tmp2 = template
                tmp2 = tmp2.replace("<step>D</step>", "<step>{}</step>".format(name))
                tmp2 = tmp2.replace("<octave>4</octave>", "<octave>{}</octave>".format(octave))
                tmp2 = tmp2.replace("<duration>1</duration>", "<duration>{}</duration>".format(duration))
                tmp2 = tmp2.replace("<type>128th</type>", "<type>{}</type>".format(ty))
                out_fname = "{}.xml".format(i)
                with open(out_fname, "w") as output:
                    output.write(tmp2)
                lbs.write("{} {} {} {} {}\n".format(out_fname, name, octave, duration, ty))
                i += 1
    
lbs.close()
