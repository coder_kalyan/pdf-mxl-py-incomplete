lbs = open("labels.txt", "w")
steps = ["A","B","C", "D", "E", "F", "G"]
step_pos = 6
step = steps[step_pos]
octave = 3
combs_1 = []
types = ["16th","eighth","quarter","half","whole"]
with open("template.xml", "r") as temp_f:
    template = temp_f.read()
    i = 0
    while True:
        for duration in range(3, 8):
            combs_1.append(step + str(octave))
            tmp2 = template
            tmp2 = tmp2.replace("<step>D</step>", "<step>{}</step>".format(step))
            tmp2 = tmp2.replace("<octave>4</octave>", "<octave>{}</octave>".format(octave))
            tmp2 = tmp2.replace("<duration>1</duration>", "<duration>{}</duration>".format(duration))
            # print(types, duration - 3)
            tmp2 = tmp2.replace("<type>128th</type>", "<type>{}</type>".format(types[duration - 3]))
            out_fname = "{}.xml".format(i)
            with open(out_fname, "w") as output:
                output.write(tmp2)
            lbs.write("{} {} {} {} {}\n".format(out_fname, step, octave, duration, types[duration - 3]))
            print(step + str(octave))
        step_pos += 1
        if step_pos >= len(steps):
            step_pos = 0
        step = steps[step_pos]
        i += 1
        if step == 'C':
            octave += 1
        if step == 'D' and octave == 6:
            lbs.close()
            exit(0)
            
# lbs.close()
