import os
import pandas as pd

source_dirs = ["/input_png_b/", "/input_png_e/", "/input_png_g/"]
cwd = os.getcwd()
dest_dir = cwd + "/input/"

df = pd.read_table("labels.txt", delim_whitespace = True, header = None)
class_names = map(lambda x : x.split(".")[0] + ".png", df.iloc[:, 0].values)
label_names = pd.Series(df.iloc[:,1].values).str.cat([map(str, df.iloc[:,2].values), map(str,df.iloc[:,3].values)]).values
zipped_map = zip(class_names, label_names)

for file_name in zipped_map:
	if os.path.exists(dest_dir + file_name[1]):
		continue
	os.mkdir(dest_dir + file_name[1])
	for i, dir_name in enumerate(source_dirs):
		current_dir = cwd + dir_name
		os.system("cp " + current_dir + file_name[0] + " " + dest_dir + file_name[1] + "/" + file_name[0].split(".")[0] + str(i + 1) + ".png")
		
