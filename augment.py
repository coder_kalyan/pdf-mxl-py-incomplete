import Augmentor
import os

for class_dir in os.listdir("input"):
	p = Augmentor.Pipeline("input/" + class_dir, save_format="PNG")
	p.zoom(probability=0.7, min_factor=0.8,max_factor=1.5)
	p.rotate(probability=0.7,max_left_rotation=5,max_right_rotation=5)
	p.random_distortion(probability=0.5,grid_width=4, grid_height=4, magnitude=8)
	p.sample(1000)
